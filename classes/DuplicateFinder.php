<?php

class DuplicateFinder
{

    private $consoleData = null;
    private $resultFilePath = '';
    private $resultFileResource = null;
    private $resultFileName = 'results.txt';

    /* Search by filename and size by default*/
    private $selectedSearchType = 'default';

    /* Temp array */
    private $duplicateData = [
        'default'    => [],
        'onlyByHash' => [],
        'onlyByName' => []
    ];

    /* Current script files - auto fill array*/
    private $scriptFiles = [];

    /**
     * Allowed input parameters.
     */
    private $inputParams = [
        'path'           => '',
        'maxFileSize'    => 0,
        'recursive'      => 1,
        'onlyByName'     => 0,
        'onlyByHash'     => 0,
        'sync'           => 1,
        /* Skip READ_FOLDER_ERROR and READ_FILE_ERROR messages */
        'skipReadError'  => 0
    ];

    const LIMIT_INPUT_PARAMS = 'The limit of admissible parameters is exceeded!';
    const GET_PATH_ERROR = 'Can\'t get current path!';
    const CREATE_RESULT_FILE_ERROR = 'Can\'t open or create result file!';
    const LOCK_RESULT_FILE_ERROR = 'Can\'t lock result file!';
    const READ_FOLDER_ERROR = 'Can\'t read folder: ';
    const READ_FILE_ERROR = 'Can\'t read file: ';
    const READ_FILE_SIZE_ERROR = 'Can\'t read size for file: ';
    const GET_FILE_HASH_ERROR = 'Can\'t read hash for file: ';

    /**
     * Construct function.
     * @param $consoleData - Console request data.
     */
    function __construct($consoleData)
    {
        $this->inputParams['path'] = getcwd();
        if(! $this->inputParams['path']){
            die(self::GET_PATH_ERROR);
        }

        $this->resultFilePath = $this->resultFilePath ?
            $this->resultFilePath . '/' . $this->resultFileName :
            $this->inputParams['path'] . '/' . $this->resultFileName;

        $this->scriptFiles = [
            $this->resultFilePath,
            $this->inputParams['path']  . '/index.php',
            $this->inputParams['path']  . '/classes/' . basename(__FILE__)
        ];

        /* Update input params */
        if($consoleData !== null){
            $this->consoleData = $consoleData;

            $this->parseConsoleParams();
        } else {
            $this->parseGetParams();
        }

        $this->openResultFile();

        if($this->inputParams['sync']){
            $this->lockResultFile();
        }

        $this->detectSearchType();

        $this->scanDir($this->inputParams['path']);

        $this->processScanResult();

        $this->duplicateData = null;
        fclose($this->resultFileResource);
    }

    /**
     * Work with data after search.
     */
    function processScanResult(){

        foreach (
            $this->duplicateData[$this->selectedSearchType] as $fileName =>
            $data
        ) {

            $duplicateFileIndexes = [];

            $fileCount = count($data);
            if ($fileCount > 1) {

                for ($i = 0; $i < $fileCount; $i++) {
                    for ($j = $fileCount - 1; $j > -1; $j--) {

                        if ($i != $j && (
                            $data[$i]['size'] == $data[$j]['size'] ||
                            $this->selectedSearchType == 'onlyByName' ||
                            $this->selectedSearchType == 'onlyByHash')
                        ) {

                            $pathJ = $this->selectedSearchType == 'onlyByHash' ?
                                $data[$j]['path'] . PHP_EOL :
                                $data[$j]['path'] . $fileName . PHP_EOL;

                            $pathI = $this->selectedSearchType == 'onlyByHash' ?
                                $data[$i]['path'] . PHP_EOL :
                                $data[$i]['path'] . $fileName . PHP_EOL;

                            if (empty($duplicateFileIndexes)) {

                                fwrite($this->resultFileResource, $pathJ);
                                $duplicateFileIndexes[] = $i;
                                $duplicateFileIndexes[] = $j;

                            } else {

                                if (! in_array($i, $duplicateFileIndexes)) {
                                    fwrite($this->resultFileResource, $pathI);
                                    $duplicateFileIndexes[] = $i;
                                }
                                if (! in_array($j, $duplicateFileIndexes)) {
                                    fwrite($this->resultFileResource, $pathJ);
                                    $duplicateFileIndexes[] = $j;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Get file hash.
     * @param $filePath string - File path.
     * @return string - File hash.
     */
    function getFileHash ($filePath){

        $hash = md5_file($filePath);

        if(! $hash){
            die(self::GET_FILE_HASH_ERROR . $filePath);
        }

        return $hash;
    }

    /**
     * Scan dir function.
     * @param $path string - Path to scan.
     */
    function scanDir($path){

        $lastChar = substr($path, -1);

        if(! (
            $lastChar == '/' ||
            $lastChar == '\\'
        )){
            $path .= '/';
        }

        if(! is_readable($path)){
            if($this->inputParams['skipReadError']){
                return;
            }
            die(self::READ_FOLDER_ERROR . $path);
        }

        $scanResult = scandir($path);

        foreach ($scanResult as $itemName){
            if($itemName != '.' && $itemName != '..'){
                if(is_dir($path . $itemName)){
                    if($this->inputParams['recursive'] == 1){
                        $this->scanDir($path . $itemName);
                    }
                } else {
                    if(! is_readable($path . $itemName)){
                        if($this->inputParams['skipReadError']){
                            return;
                        }
                        die(self::READ_FILE_ERROR . $path);
                    }
                    $this->processFile($path, $itemName);
                }
            }
        }
    }

    /**
     * Get file info.
     * @param $path string - File path.
     * @param $itemName string - File name.
     */
    function processFile($path, $itemName)
    {
        /* Exclude script files */
        if(! in_array($path . $itemName, $this->scriptFiles)){

            $arrayKey = $itemName;

            if($this->selectedSearchType == 'onlyByHash'){
                $arrayKey = $this->getFileHash($path . $itemName);
            }

            if(! isset($this->duplicateData[$this->selectedSearchType][$arrayKey])){
                $this->duplicateData[$this->selectedSearchType][$arrayKey] = [];
            }

            $fileSize = filesize($path . $itemName);

            if(! $fileSize){
                die(self::READ_FILE_SIZE_ERROR . $path . $itemName);
            }

            if(
                $this->inputParams['maxFileSize'] == 0 ||
                ($this->inputParams['maxFileSize'] > 0 && $fileSize <= $this->inputParams['maxFileSize'])
            ){
                $this->duplicateData[$this->selectedSearchType][$arrayKey][] = [
                    'size' => $fileSize,
                    'path' => $this->selectedSearchType == 'onlyByHash' ? $path . $itemName : $path
                ];
            }
        }
    }

    /**
     * Detect search type.
     */
    function detectSearchType(){
        foreach ($this->duplicateData as $searchType => $value){
            if(isset($this->inputParams[$searchType]) && $this->inputParams[$searchType] == 1){
                $this->selectedSearchType = $searchType;
                return;
            }
        }
    }

    /**
     * Open result file.
     */
    function openResultFile(){
        $this->resultFileResource = fopen($this->resultFilePath, 'w+');

        if(! $this->resultFileResource){
            die(self::CREATE_RESULT_FILE_ERROR);
        }
    }

    /**
     * Lock result file.
     */
    function lockResultFile(){
        if(! flock($this->resultFileResource, LOCK_EX)){
            die(self::LOCK_RESULT_FILE_ERROR);
        }
    }

    /**
     * Parse console params function.
     */
    function parseConsoleParams(){
        if(! empty($this->consoleData)){
            $this->checkInputLimit($this->consoleData);
            unset($this->consoleData[0]);

            foreach ($this->consoleData as $value){
                $paramData = explode('=', $value);
                $paramData[0] = str_replace('--', '', $paramData[0]);

                if(isset($this->inputParams[$paramData[0]])){
                    $this->inputParams[$paramData[0]] = $paramData[1]; /* Need checking param type/length */
                }
            }
        }
    }

    /**
     * Parse get params function.
     */
    function parseGetParams(){
        if(! empty($_GET)) {
            $this->checkInputLimit($_GET);

            foreach ($_GET as $paramName => $value){
                if(isset($this->inputParams[$paramName])){
                    $this->inputParams[$paramName] = $value; /* Need checking param type/length */
                }
            }
        }
    }

    /**
     * Check input limit function.
     * @param $inputParams - Input values.
     */
    function checkInputLimit($inputParams){
        if(count($inputParams) > count($this->inputParams)){
            die(self::LIMIT_INPUT_PARAMS);
        }
    }
}