# Duplicate finder

Search duplicate files in the folder and write their paths to the file.
Tested on Windows and Linux.

# How to use

**In command line:**

Switch to the project folder and type:
    
    php index.php
    
By default program search files by **name** and **size** in the current path.

You can set this params ($inputParams in DuplicateFinder class):

        --path           - (string); default - current folder; Path to search folder.
        --maxFileSize    - (int);    default - 0, any sizes;   Max size of finded files.
        --recursive      - (int);    default - 1;              Recursive search.
        --onlyByName     - (int);    default - 0;              Search only by name.
        --onlyByHash     - (int);    default - 0;              Search only by file hash.
        --sync           - (int);    default - 1;              Waits for the previous search to end. 
        --skipReadError  - (int);    default - 0;              Ignore, if some files or directories can't be read.

Params **onlyByName** and **onlyByHash** can't be use together. First specified param has more priority.

If you leave **path** param by default, program will **exclude** scripts files from search.

If program was finished search without errors, you can see finished message like this:

    Processing time: 0.01 sec.

If you will have some permission problems, you receive message.

**In browser:**

Just run your script in browser like:

    http://localhost/index.php

And use the same params like this:

    http://localhost/index.php?maxFileSize=123
    http://localhost/index.php?onlyByName=1
    http://localhost/index.php?onlyByHash=1
    
_Note: You can use path param with space in browser without '' or "" like this:_

http://localhost/index.php?path=D:\te st
  
**Result:**

You can see duplicate file paths in file **results.txt**. It wll be created in project folder. You can change this path in DuplicateFinder class **$resultFilePath**.