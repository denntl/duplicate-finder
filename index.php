<?php

$start = microtime(true);

require "classes/DuplicateFinder.php";

$duplicateFinder = new DuplicateFinder($argv);

echo 'Processing time: ' . (round(microtime(true) - $start, 2)) . ' sec.' . PHP_EOL;